package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.exceptions.UserDoNotExistException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.dto.LoginDto;
import com.crm.backend.crm.model.dto.PageResponse;

import java.util.List;
import java.util.Optional;

public interface IAppUserService {

    void register(AppUser appUser) throws RegistrationException;

    PageResponse<AppUser>getAllUsers();
    PageResponse<AppUser> getUsers(int page);

    List<CrmClient> getAllClientsOfUser(Long userID);

    Optional<AppUser> getUserById(Long userId);

    void deleteUser(AppUser appUser);

    Optional<AppUser> getUserWithLoginAndPassword(LoginDto loginDto)  throws UserDoNotExistException;
}
