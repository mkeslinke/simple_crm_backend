package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.dto.PageResponse;

import java.util.List;
import java.util.Optional;

public interface ICrmClientService {


    void deleteClient(CrmClient crmClient);

    CrmClient getCasesOfClient(Long clientId);
    Optional<CrmClient> register(CrmClient crmClient) throws RegistrationException;


}
