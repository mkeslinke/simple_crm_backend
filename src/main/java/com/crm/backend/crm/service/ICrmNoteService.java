package com.crm.backend.crm.service;


import com.crm.backend.crm.exceptions.NoteAlreadyExistException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmNote;

import java.util.Optional;

public interface ICrmNoteService {

    public void addNote(CrmNote crmNote) throws RegistrationException;

    Optional<CrmNote> createNote(CrmNote crmNote) throws NoteAlreadyExistException;

    void deleteNote(CrmNote crmNote);
}
