package com.crm.backend.crm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MessageResponse extends Response {
    private String message;
}
