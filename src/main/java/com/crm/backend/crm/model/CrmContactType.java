package com.crm.backend.crm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmContactType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long contactTypeId;

    private String name;

    public CrmContactType(String name) {
        this.name = name;
    }
}
