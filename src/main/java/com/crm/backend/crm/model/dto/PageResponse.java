package com.crm.backend.crm.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@NoArgsConstructor
public class PageResponse<T> extends Response {
    private List<T>objects;
    private int currentPage;
    private int totalElement;


    public PageResponse(Page<T> objects){
        this.currentPage = objects.getNumber();
        this.totalElement = objects.getNumberOfElements();
        this.objects = objects.getContent();
    }

}
