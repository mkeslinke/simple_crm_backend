package com.crm.backend.crm.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmClientDto {
    private String clientName;
    private String phoneNumber;
    private String email;
    private String city;
    private Long idUser;
}
