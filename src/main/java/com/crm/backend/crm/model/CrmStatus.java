package com.crm.backend.crm.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long statusId;

    private String name;

    public CrmStatus(String name) {
        this.name = name;
    }
}
