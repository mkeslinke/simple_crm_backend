package com.crm.backend.crm.model.dto;

import com.crm.backend.crm.model.AppUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationDto {

    private String token;
    private AppUser user;
}
