package com.crm.backend.crm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmCase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long caseId;

    private String caseDescription;

    @OneToMany()
    private List<CrmNote> crmNoteList;

    @OneToMany()
    private List<CrmProduct> crmProductList;

    @OneToOne
    private CrmStatus crmStatusList;


    public CrmCase(String caseDescription) {
        this.caseDescription = caseDescription;
    }
}
