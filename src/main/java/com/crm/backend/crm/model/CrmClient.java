package com.crm.backend.crm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmClient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long clientId;

    public CrmClient(String clientName, String phoneNumber, String email, String city) {
        this.clientName = clientName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.city = city;
    }

    private String clientName;
    private String phoneNumber;
    private String email;
    private String city;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private List<CrmCase> crmCaseList;

    @OneToMany
    private List<CrmReminder> crmReminderList;

}
