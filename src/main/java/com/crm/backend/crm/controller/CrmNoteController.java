package com.crm.backend.crm.controller;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.model.CrmNote;
import com.crm.backend.crm.model.dto.Response;
import com.crm.backend.crm.model.dto.ResponseFactory;
import com.crm.backend.crm.repository.CrmCaseRepository;
import com.crm.backend.crm.repository.CrmNoteRepository;
import com.crm.backend.crm.service.ICrmNoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/note")
public class CrmNoteController {
    @Autowired
    private ICrmNoteService crmNoteService;

    @Autowired
    private CrmNoteRepository crmNoteRepository;

    @Autowired
    private CrmCaseRepository crmCaseRepository;


    @Autowired
    public CrmNoteController(ICrmNoteService crmNoteService) {
        this.crmNoteService = crmNoteService;
    }

    @RequestMapping(path= "/addnote", method = RequestMethod.POST)
    public ResponseEntity addNote (@RequestParam ("caseid") Long caseId,
                                   @RequestParam ("noteid") Long noteId){
        Optional<CrmCase> crmCaseOptional = crmCaseRepository.findById(caseId);
        if (crmCaseOptional.isPresent()){
            CrmCase crmCase = crmCaseOptional.get();

            Optional<CrmNote> crmNoteOptional = crmNoteRepository.findCrmNoteByNoteId(noteId);
            if (crmNoteOptional.isPresent()){
                CrmNote crmNote = crmNoteOptional.get();


            crmCase.getCrmNoteList().add(crmNote);
            crmCaseRepository.save(crmCase);
            return ResponseFactory.created();
            }
        }
        return ResponseFactory.badRequest();
    }



    @RequestMapping(path = "/create", method = RequestMethod.GET)
    public ResponseEntity createNote(@RequestParam ("description") String description)throws RegistrationException{
        Optional<CrmNote> crmNoteOptional = crmNoteService.createNote(new CrmNote(description)) ;
        if (crmNoteOptional.isPresent()){
            return ResponseFactory.ok(crmNoteOptional.get().getNoteId() + "");
        }else {
            return ResponseFactory.badRequest();
        }

    }

    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
    public void deleteNote(@RequestParam ("noteid") Long noteid){
        Optional<CrmNote> crmNoteOptional = crmNoteRepository.findById(noteid);
        if (crmNoteOptional.isPresent()) {
            CrmNote crmNote = crmNoteOptional.get();
            crmNoteService.deleteNote(crmNote);
        }
    }
}