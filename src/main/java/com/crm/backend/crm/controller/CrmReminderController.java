package com.crm.backend.crm.controller;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.CrmReminder;
import com.crm.backend.crm.model.dto.ResponseFactory;
import com.crm.backend.crm.repository.AppUserRepository;
import com.crm.backend.crm.repository.CrmClientRepository;
import com.crm.backend.crm.repository.CrmReminderRepository;
import com.crm.backend.crm.service.ICrmNoteService;
import com.crm.backend.crm.service.ICrmReminderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/reminders/")
public class CrmReminderController {

    @Autowired
    private ICrmReminderService crmReminderService;
    @Autowired
    private CrmReminderRepository crmReminderRepository;

    @Autowired
    private CrmClientRepository crmClientRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    public CrmReminderController(ICrmReminderService crmReminderService) {
        this.crmReminderService = crmReminderService;
    }

    @RequestMapping(value = "/createreminder", method = RequestMethod.GET)
    public ResponseEntity createReminder(@RequestParam("contactdate") String contactDate,
                                         @RequestParam("remindernote") String reminderNote, boolean isChecked) throws RegistrationException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(contactDate, formatter);

        Optional<CrmReminder> crmReminderOptional = crmReminderService.createReminder(
                new CrmReminder(date, reminderNote, isChecked));
        if (crmReminderOptional.isPresent()) {
            return ResponseFactory.ok(crmReminderOptional.get().getReminderId() + "");
        } else {
            return ResponseFactory.badRequest();
        }
    }

    @RequestMapping(value = "/addreminder", method = RequestMethod.GET)
    public ResponseEntity addReminderToClient(@RequestParam("clientid") Long clientId,
                                              @RequestParam("reminderid")Long reminderId){

        Optional<CrmClient> crmClientOptional = crmClientRepository.findByClientId(clientId);
        if (crmClientOptional.isPresent()) {
            CrmClient crmClient = crmClientOptional.get();


            Optional<CrmReminder> crmReminderOptional = crmReminderRepository.findCrmReminderByReminderId(reminderId);
            if (crmReminderOptional.isPresent()) {
                CrmReminder crmReminder = crmReminderOptional.get();

                crmClient.getCrmReminderList().add(crmReminder);
                crmClientRepository.save(crmClient);
                return ResponseFactory.created();          }
        }
        return ResponseFactory.badRequest();
    }

    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
    public void deleteReminder(@RequestParam ("reminderid") Long reminderId){
        Optional<CrmReminder> crmReminderOptional = crmReminderRepository.findById(reminderId);
        if (crmReminderOptional.isPresent()){
            CrmReminder crmReminder = crmReminderOptional.get();
            crmReminderService.deleteReminder(crmReminder);
        }
    }

    public void checkReminder(@RequestParam ("clientid") Long clientId,
                              @RequestParam ("reminderid") Long reminderId){
        crmReminderRepository.findCrmReminderByReminderId(reminderId);

    }


//
//    public void setReminderAsDone(@RequestParam ("userid") Long userId,
//                                 @RequestParam ("reminderid") Long reminderId){
//        Optional<CrmReminder> crmReminderOptional = crmReminderRepository.findById(reminderId);
//
//        if (crmReminderOptional.isPresent()){
//            CrmReminder crmReminder = crmReminderOptional.get();
//            Optional<AppUser> appUserOptional = appUserRepository.findById(userId);
//            AppUser appUser = appUserOptional.get();
//            Optional<CrmClient> crmClientOptional = crmClientRepository.findClientByReminderId(reminderId);
//            CrmClient crmClient = crmClientOptional.get();
//            }
//        }
//    }
//
//  TODO:   : zrobić zapytanie ustawiające reminder jako wykonany.
//    Podajemy id użytkownika oraz id remindera, a następnie sprawdzamy czy dany reminder należy do klienta który należy do użytkownika

}
