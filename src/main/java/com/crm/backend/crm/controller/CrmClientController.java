package com.crm.backend.crm.controller;


import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.CrmReminder;
import com.crm.backend.crm.model.dto.Response;
import com.crm.backend.crm.model.dto.ResponseFactory;
import com.crm.backend.crm.repository.AppUserRepository;
import com.crm.backend.crm.repository.CrmCaseRepository;
import com.crm.backend.crm.repository.CrmClientRepository;
import com.crm.backend.crm.repository.CrmReminderRepository;
import com.crm.backend.crm.service.IAppUserService;
import com.crm.backend.crm.service.ICrmClientService;
import com.crm.backend.crm.service.ICrmReminderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping("/clients/")
public class CrmClientController {

    @Autowired
    private CrmCaseRepository crmCaseRepository;

    @Autowired
    private ICrmClientService crmClientService;
    @Autowired
    private CrmClientRepository crmClientRepository;
    @Autowired
    private IAppUserService appUserService;
    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private CrmReminderRepository crmReminderRepository;

    @Autowired
    private ICrmReminderService crmReminderService;


    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity registerClient(@RequestBody CrmClient crmClient) throws RegistrationException {
        crmClientService.register(crmClient);

        return ResponseFactory.created();

    }


    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public ResponseEntity registerClient(@RequestParam("name") String clientName,
                                         @RequestParam("mail") String clientMail,
                                         @RequestParam("phone") String clientPhone,
                                         @RequestParam("city") String city) throws RegistrationException {
        Optional<CrmClient> registered = crmClientService.register(new CrmClient(clientName, clientPhone, clientMail, city));

        if (registered.isPresent()) {
            return ResponseFactory.ok(registered.get().getClientId() + "");
        } else {
            return ResponseFactory.badRequest();
        }
    }

    @RequestMapping(path = "/add", method = RequestMethod.GET)
    public ResponseEntity addClient(@RequestParam("userid") Long userId,
                                    @RequestParam("clientid") Long clientId) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(userId);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();


            Optional<CrmClient> crmClientOptional = crmClientRepository.findById(clientId);
            if (crmClientOptional.isPresent()) {
                CrmClient crmClient = crmClientOptional.get();

                appUser.getCrmClientList().add(crmClient);
                appUserRepository.save(appUser);
                return ResponseFactory.created();
            }
        }
        return ResponseFactory.badRequest();
    }


    @RequestMapping(path = "/deleteclient", method = RequestMethod.DELETE)
    public void deleteClient(@RequestParam("clientid") Long clientid) throws RegistrationException {

        Optional<CrmClient> crmClientOptional = crmClientRepository.findById(clientid);
        if (crmClientOptional.isPresent()) {
            CrmClient crmClient1 = crmClientOptional.get();
            crmClientService.deleteClient(crmClient1);
        }
    }


    @RequestMapping(path = "/clientinfo", method = RequestMethod.GET)
    public ResponseEntity getClientInfo(@RequestParam(name = "clientid") Long clientId) {

        CrmClient client = crmClientService.getCasesOfClient(clientId);

        return ResponseFactory.result(client);
    }


    @RequestMapping(path = "/edit", method = RequestMethod.GET)
    public ResponseEntity editClient(@RequestParam("clientid") Long clientId,
                                     @RequestParam("name") String clientName,
                                     @RequestParam("phone") String clientPhone,
                                     @RequestParam("mail") String mail,
                                     @RequestParam("city") String city) {

        Optional<CrmClient> crmClientOptional = crmClientRepository.findById(clientId);
        if (crmClientOptional.isPresent()) {
            CrmClient crmClient = crmClientOptional.get();
            crmClient.setClientName(clientName);
            crmClient.setCity(city);
            crmClient.setPhoneNumber(clientPhone);
            crmClient.setEmail(mail);
            crmClientRepository.save(crmClient);
            return ResponseFactory.created();
        }
        return ResponseFactory.badRequest();

    }

    @RequestMapping(path = "/getreminders", method = RequestMethod.GET)
    public List<CrmReminder> getRemindersForUser(@RequestParam("userid") Long userId) {
        List<CrmClient> clientList = appUserService.getAllClientsOfUser(userId);
        List<CrmReminder> reminderList = new ArrayList<>();
        for (int i = 0; i < clientList.size(); i++) {
            reminderList.addAll(clientList.get(i).getCrmReminderList());
        }

        return reminderList;
    }



}