package com.crm.backend.crm.component;

import com.crm.backend.crm.exceptions.ProductAlreadyExsistException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmContactType;
import com.crm.backend.crm.model.CrmProduct;
import com.crm.backend.crm.model.CrmStatus;
import com.crm.backend.crm.repository.AppUserRepository;
import com.crm.backend.crm.repository.CrmContactTypeRepository;
import com.crm.backend.crm.repository.CrmProductRepository;
import com.crm.backend.crm.repository.CrmStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    //private AppUserRepository appUserRepository;
    private CrmContactTypeRepository crmContactTypeRepository;
    private CrmStatusRepository crmStatusRepository;
    private CrmProductRepository crmProductRepository;

    @Autowired
    public DataInitializer(CrmContactTypeRepository crmContactTypeRepository, CrmStatusRepository crmStatusRepository, CrmProductRepository crmProductRepository) {
        this.crmContactTypeRepository = crmContactTypeRepository;
        this.crmStatusRepository = crmStatusRepository;
        this.crmProductRepository = crmProductRepository;

        loadContactTypes();
        loadStatuses();
        loadProducts();
    }

    private void loadProducts()  {
//        crmProductRepository.save(new CrmProduct("Jabłko"));
//        crmProductRepository.save(new CrmProduct("Malina"));
//        crmProductRepository.save(new CrmProduct("Banan"));
//        crmProductRepository.save(new CrmProduct("Gruszka"));
//        crmProductRepository.save(new CrmProduct("Pomarańcza"));
    }

    private void loadStatuses() {
//        crmStatusRepository.save(new CrmStatus("Podjęcie"));
//        crmStatusRepository.save(new CrmStatus("W trakcie"));
//        crmStatusRepository.save(new CrmStatus("Finalizacja"));
//        crmStatusRepository.save(new CrmStatus("Zakończenie"));
    }

    private void loadContactTypes() {
//        crmContactTypeRepository.save(new CrmContactType("Pierwszy telefon"));
//        crmContactTypeRepository.save(new CrmContactType("Pierwsze spotkanie"));
//        crmContactTypeRepository.save(new CrmContactType("Kolejny telefon"));
//        crmContactTypeRepository.save(new CrmContactType("Kolejne spotkanie"));
    }

}