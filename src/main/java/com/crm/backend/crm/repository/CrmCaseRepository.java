package com.crm.backend.crm.repository;

import com.crm.backend.crm.model.CrmCase;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CrmCaseRepository extends JpaRepository<CrmCase, Long> {

    Optional<CrmCase> findCrmCaseByCaseDescription(String caseDescription);

}
