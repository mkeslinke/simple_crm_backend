package com.crm.backend.crm.repository;
import com.crm.backend.crm.model.CrmClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CrmClientRepository extends JpaRepository<CrmClient, Long>{

    Optional<CrmClient> findByClientName(String clientname);
    Optional<CrmClient> findByClientId(Long id);
    Page<CrmClient> findAllBy (Pageable pageable);
}
