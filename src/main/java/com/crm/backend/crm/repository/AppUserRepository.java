package com.crm.backend.crm.repository;

import com.crm.backend.crm.model.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByLogin(String login);
    Optional<AppUser> findByEmail(String email);
    Page<AppUser> findAllBy(Pageable pageable);
//    Optional<AppUser> findByLogin(String username);


}
