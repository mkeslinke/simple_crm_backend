package com.crm.backend.crm.repository;

import com.crm.backend.crm.model.CrmContactType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrmContactTypeRepository extends JpaRepository<CrmContactType, Long> {


}
