package com.crm.backend.crm.repository;

import com.crm.backend.crm.model.CrmReminder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface CrmReminderRepository extends JpaRepository<CrmReminder, Long> {

    Optional<CrmReminder> findCrmReminderByReminderId(Long reminderId);
    Optional<CrmReminder> findCrmReminderByReminderNote(String reminderNote);



    Page<CrmReminder> findAllBy (Pageable pageable);

}
